#!/usr/bin/env perl
use strict;
use warnings;

use lib 'lib';

use Test::More tests => 2;
use App::WRT;

chdir 'example/blog';

my $w = App::WRT::new_from_file('wrt.json');

my $result = $w->display('filter_test');

ok(
  $result =~ m/id="filter_test-heading-level-2"/,
  'Got a heading with an id'
);

ok(
  $result =~ m/href="#filter_test-heading-level-2"/,
  'Got a link to the id'
);
